import ie.ucd.items.*;
import ie.ucd.people.*;

public class NonDrinker extends Person{
	
	//Constructor
	public NonDrinker(){
		
	}

	public boolean drink(Drink drink) {
		if(drink instanceof AlcoholicDrink) {
			System.out.println("Will not drink alcohol");
			return false;
		}else{	
			System.out.println("Drinking non-alcoholic beverage");
		return true;
		}
	}
	
	
	public boolean eat(Food food){
		return true;
	}
	
	public void isDrunk() {
		System.out.println("Not drunk");
	}	
}


