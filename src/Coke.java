import ie.ucd.items.NotAlcoholicDrink;

public class Coke extends NotAlcoholicDrink {
	
//Constructor for a non-alcoholic class
	public Coke(String name, double volume) {
		super(name, volume);
	}

}
