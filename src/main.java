import ie.ucd.items.*;
import ie.ucd.people.*;


public class main {

	public static void main(String[] args) {

		//Creating objects		
		Wine myWine = new Wine("Barolo", 1, 1, WineType.Red);
		Coke myCoke = new Coke("CocaCola", 1);
		Drinker myDrinker = new Drinker();		
		NonDrinker myNonDrinker = new NonDrinker();
		
		
		//Testing drinker 
		myDrinker.isDrunk();
		myDrinker.setWeight(80.00);
		System.out.println(myDrinker.getWeight());
		myDrinker.drink(myWine);
		myDrinker.drink(myCoke);
		myDrinker.isDrunk();
		for(int i=1; i<11; i++){
			myDrinker.drink(myWine);
		}
		myDrinker.isDrunk();
		
		
		
		//Non-drinker testing
		myNonDrinker.isDrunk();
		myNonDrinker.setWeight(70.00);
		System.out.println(myNonDrinker.getWeight());
		myNonDrinker.drink(myWine);
		myNonDrinker.drink(myCoke);
		myNonDrinker.isDrunk();
		
	}

}
