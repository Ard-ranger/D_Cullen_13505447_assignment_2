import ie.ucd.items.*;
import ie.ucd.people.*;

public class Drinker extends Person{
	

private int numberOfDrinks;	
	
//Constructor
	public Drinker(){

	}

	public boolean drink(Drink drink) {
		if(drink instanceof AlcoholicDrink) {
			System.out.println("Drinking alcohol");		
			numberOfDrinks++;
			System.out.println("The total number of drinks is: " + numberOfDrinks);
		}
		return true;
	}
	
	
	public boolean eat(Food food){
		return true;
	}
	
	
	public void isDrunk() {
		if((super.getWeight()/10.00) < numberOfDrinks){
			System.out.println("Drunk");
		}else {
			System.out.println("Not drunk");
		}
	}	
}
